# Sección 2: Introducción a JSX.

La practica de esta sección fue relativamente sencilla: sin embargo esta implicada por algo de teoría de por medio. 

Una de las primeras cosas que se nos explican es la sintaxis o constitución de los componentes _reactivos_: los cuales
parecen etiquetas de HTML. Posteriormente se nos explica que estas etiquetas tambien tienen componentes y sus reglas:

```javascript
const Components = () => {
    return (
        <View>
            <Text style = {estilos.headerOne}>¡He Empezado con React Native!</Text>
            <Text style = {estilos.headerTwo}>Atte: El Gio UwU</Text>
        </View>
    );
}

const estilos = StyleSheet.create({
    headerOne: {
        fontSize: 45
    },
    headerTwo: {
        fontSize: 20
    }
});
```
Basicamente la practica se centra en el código mostrado arriba: se nos explica que podemos acceder a las variables de Javascript desde componentes JSX 
(como en el caso de `style = {estilos.headerOne}`). Posteriormente el autor nos dice que la funcion `StyleSheet.create()` solo valida que el esquema de datos
sea valido en cuanto a los atributos que podemos describir.

Notas:
- de la libreria `react-native` importamos los componentes primitivos.
- de la libreria `react` importamos lo que nos permite manipular el comportamiento de los componentes.
