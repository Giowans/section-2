import { createStackNavigator, createAppContainer } from 'react-navigation';
import HomeScreen from './src/screens/HomeScreen';
import Components from './src/screens/componentScreen';

const navigator = createStackNavigator(
  {
    Home: HomeScreen,
    Componentes: Components
  },
  {
    initialRouteName: 'Componentes',
    defaultNavigationOptions: {
      title: 'rn-starter'
    }
  }
);

export default createAppContainer(navigator);
